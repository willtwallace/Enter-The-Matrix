# Enter-The-Matrix

(A WIP) Tiny javascript file that replicates the "matrix" effect

[Working JSFiddle Example #1 With DecodeURI](https://jsfiddle.net/willtwallace/5s0yLm3e/2/)

[Working JSFiddle Example #2 With Integer Binary](https://jsfiddle.net/willtwallace/pq54oghu/4/)

_Updated JSFiddle with `<body onload="enterTheMatrix()">` since we arent calling an actual JS file in the examples_


Dependencies: jQuery (Latest) - https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js



## Setup

### Step 1: Link the file

```
<script src="matrix.js" type="text/javascript">
```

### Step 2: Set the selector ( matrix )

```
<div id="matrix"></div>
```
Note: 2894ef8da132cdc9a66d14e712633fb53b42d570

### Step 3: Call the function

```
enterTheMatrix();
```

## Example

### HTML

Note: If you wish to use nested div's its a tad bit more involved; you can follow something like this:

```
<div class="container-fluid" id="header">

    <div id="matrix"></div>

</div>
```

### CSS

Note: Your parent container must span the size of the area you wish to populate, whether it be 25%, 250px, 50%, 100%, etc.

```
home, body {
    height: 100%;
    width: 100%;
}

#matrix {
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    color: green;
    z-index: -999;
}

#matrix p {
    position: absolute;
}
```

Nice and Clean!
