//        ___           ___                       ___                       ___     
//       /__/\         /  /\          ___        /  /\        ___          /__/|    
//      |  |::\       /  /::\        /  /\      /  /::\      /  /\        |  |:|    
//      |  |:|:\     /  /:/\:\      /  /:/     /  /:/\:\    /  /:/        |  |:|    
//    __|__|:|\:\   /  /:/~/::\    /  /:/     /  /:/~/:/   /__/::\      __|__|:|    
//   /__/::::| \:\ /__/:/ /:/\:\  /  /::\    /__/:/ /:/___ \__\/\:\__  /__/::::\____
//   \  \:\~~\__\/ \  \:\/:/__\/ /__/:/\:\   \  \:\/:::::/    \  \:\/\    ~\~~\::::/
//    \  \:\        \  \::/      \__\/  \:\   \  \::/~~~~      \__\::/     |~~|:|~~ 
//     \  \:\        \  \:\           \  \:\   \  \:\          /__/:/      |  |:|   
//      \  \:\        \  \:\           \__\/    \  \:\         \__\/       |  |:|   
//       \__\/         \__\/                     \__\/                     |__|/    
//
//
//  WILL WALLACE 2018 (C)
//  07.31.2018
//  15:24:00

function enterTheMatrix() {

    var chartable = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" + decodeURIComponent("%E3%83%A9%E3%83%89%E3%82%AF%E3%83%AA%E3%83%95%E3%80%81%E3%83%9E%E3%83%A9%E3%82%BD%E3%83%B3%E4%BA%94%E8%BC%AA%E4%BB%A3%E8%A1%A8%E3%81%AB%201%E4%B8%87m%E5%87%BA%E5%A0%B4%E3%81%AB%E3%82%82%E5%90%AB%E3%81%BF"); // ALL ALLOWABLE CHARACTERS TO ENTER THE MATRIX
    var clientHeight = document.getElementById("matrix").clientHeight; // DETERMINE DIV HEIGHT
    var screenWidth = $(window).width(); // USE JS TO DETERMIN USERS SCREEN WIDTH
    var rndScreenWidth = Math.floor(Math.random() * screenWidth); // RANDOMLY GENERATED NUMBER WITHIN SCREEN SIZE
    var rndFade = "1000"; // DEFAULT FADE-IN VALUE SET TO 1S OR 1000MS
    var matrixText = ""; // VAR TO HOLD OUR RANDOM MATRIX CONTENT

    // THIS FUNCTION GENERATES RANDOM CONTENT BASED OFF THE CRITERIA BELOW
    function populateMatrixContent() {

        // GENERATES RANDOM CHARACTERS BASED OFF TABLE ABOVE
        for (var i = 0; i < 1; i++) {
            matrixText = chartable.charAt(Math.floor(Math.random() * chartable.length));
        }

        // GENERATE RANDOM ANIMATION VALUES
        for (var k = 0; k < 1; k++) {
            rndFade = (Math.floor(Math.random() * 5000)); // GENERATE OUR RANDOM FADEIN VALUES
        }

    }

    function createMatrixContent() {

        for (j = 0; j < 100; j++) {

            // CALLS THE `populateMatrixContent` TO GENERATE RANDOM CONTENT ON EACH EXECUTION OF PARENT LOOP
            populateMatrixContent();

            // CREATE HTML ELEMENT WITH DYNAMIC MATRIX CONTENT
            var ptag = document.createElement("p");
            var node = document.createTextNode(matrixText);

            ptag.appendChild(node);

            // SET CLASS NAME, ID, AND DEFINE OUR STYLING TO APPLY PROGRAMATICALLY
            ptag.className = "matrixCode";
            ptag.id = j;
            ptag.style.left = Math.floor(Math.random() * screenWidth) + "px";
            ptag.style.top = Math.floor(Math.random() * clientHeight) + "px";

            var element = document.getElementById("matrix");
            element.appendChild(ptag);

            for(m = 0; m < 10; m++) {
                $("#" + j).hide().delay(Math.floor(Math.random() * 5000)).fadeIn(enterTheMatrix.rndFade);
                $("#" + j).delay(Math.floor(Math.random() * 5000)).fadeOut(enterTheMatrix.rndFade);
            }

            // console.log(matrixText); // DEBUG ONLY -- USES A LOT OF MEMORY!

        }

    }

    // This populates the Matrix (content area)
    function populateMatrix() {

        // Calls the `PopulateMatrixContent()` function from above
        // Which generates random data to populate the Matrix
        populateMatrixContent();

        createMatrixContent();

        // The following comments are to debug the japanese characters displayed on screen. In order to change them you must be fimiliar with encoding and decoding foreign characters. These comments will assist if you ever get lost.
        // console.log(encodeURIComponent("ラドクリフ、マラソン五輪代表に 1万m出場にも含み"));
        // console.log(decodeURIComponent("%E3%83%A9%E3%83%89%E3%82%AF%E3%83%AA%E3%83%95%E3%80%81%E3%83%9E%E3%83%A9%E3%82%BD%E3%83%B3%E4%BA%94%E8%BC%AA%E4%BB%A3%E8%A1%A8%E3%81%AB%201%E4%B8%87m%E5%87%BA%E5%A0%B4%E3%81%AB%E3%82%82%E5%90%AB%E3%81%BF"));
        
    }

    // FINAL CALL TO EXECUTE THE SCRIPT (idk if its necessary anymore, but im too scared to touch)
    populateMatrix();

  }
  